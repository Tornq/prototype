using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : ScriptableObject
{
    public string nameOfChar { get; set; }
    public int health { get; set; }
    public GameObject gO { get; set; }
    public Sprite sprite { get; set; }

    public Character GetClone()
    {
        return (Character)this.MemberwiseClone();
    }
}

class Program
{
    public static void Main()
    {
        Character sonic = new Character();
        sonic.nameOfChar = "Sonic";
        sonic.health = 10;
        sonic.sprite = Resources.Load<Sprite>("S");
        sonic.gO = new GameObject();
        sonic.gO.AddComponent<SpriteRenderer>().sprite = sonic.sprite;
        

        Character tails = sonic.GetClone();
        tails.nameOfChar = "Tails";
        tails.sprite = Resources.Load<Sprite>("T");
        tails.gO = new GameObject();
        tails.gO.AddComponent<SpriteRenderer>().sprite = tails.sprite;

        Character knuckles = sonic.GetClone();
        knuckles.nameOfChar = "Knuckles";
        knuckles.health = 20;
        knuckles.sprite = Resources.Load<Sprite>("K");
        knuckles.gO = new GameObject();
        knuckles.gO.AddComponent<SpriteRenderer>().sprite = knuckles.sprite;

        Debug.Log("Character1:" + sonic.nameOfChar + sonic.health);
        Debug.Log("Character2:" + tails.nameOfChar + tails.health);
        Debug.Log("Character3:" + knuckles.nameOfChar + knuckles.health);
    }
}
